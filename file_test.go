package main

import (
	"testing"
	"time"
)

func TestArchiveNaming(t *testing.T) {
	tm := time.Date(2020, 1, 1, 13, 16, 0, 0, time.UTC)
	cfg := arConfig{Prefix: "prefix", Ext: "bin"}
	tests := []struct {
		name     string
		interval time.Duration
		cfg      arConfig
		path     string
	}{
		{
			name:     "hourly",
			interval: time.Hour,
			cfg:      cfg,
			path:     "2020/001/prefix_20200101-13.bin",
		},
		{
			name:     "6-hours",
			interval: time.Hour * 6,
			cfg:      cfg,
			path:     "2020/001/prefix_20200101-12.bin",
		},
		{
			name:     "daily",
			interval: time.Hour * 24,
			cfg:      cfg,
			path:     "2020/001/prefix_20200101.bin",
		},
		{
			name:     "15-minutes",
			interval: time.Minute * 15,
			cfg:      cfg,
			path:     "2020/001/prefix_20200101-1315.bin",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			f := archiveNamer(test.interval)
			if got := f(tm, test.cfg); got != test.path {
				t.Errorf("bad path; expected %q, got %q", test.path, got)
			}
		})
	}
}

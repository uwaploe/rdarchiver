// Rdarchiver archives data messages received on one or more Redis pub-sub
// channels.
//
package main

import (
	"context"
	"flag"
	"fmt"
	"os"
	"os/signal"
	"path/filepath"
	"runtime"
	"syscall"
	"time"

	"log/slog"

	"bitbucket.org/uwaploe/rdarchiver/internal/rdsub"
	"github.com/carlmjohnson/versioninfo"
	"github.com/gomodule/redigo/redis"
	"go.etcd.io/bbolt"
	"golang.org/x/sync/errgroup"
)

const Usage = `Usage: rdarchiver [options] cfgfile

Archive messages from one or more Redis pub-sub channels.

`

var Version = "dev"
var BuildDate = "unknown"

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	qLen int = 1
)

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprint(os.Stderr, Usage)
		flag.PrintDefaults()
	}
	flag.IntVar(&qLen, "qlen", qLen,
		"size of each data record input queue")

	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s (%s)\n", os.Args[0], Version, versioninfo.Short())
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

func initLogger() *slog.Logger {
	opts := slog.HandlerOptions{}
	// Drop log timestamps when running under Systemd
	if _, ok := os.LookupEnv("JOURNAL_STREAM"); ok {
		opts.ReplaceAttr = func(groups []string, a slog.Attr) slog.Attr {
			if a.Key == slog.TimeKey {
				return slog.Attr{}
			}
			return a
		}
	}

	return slog.New(slog.NewTextHandler(os.Stderr, &opts))
}

func abort(msg string, err error, args ...any) {
	newargs := append([]any{slog.Any("err", err)}, args...)
	slog.Error(msg, newargs...)
	os.Exit(1)
}

func redisConnect(addr string) (redis.PubSubConn, error) {
	var ps redis.PubSubConn
	conn, err := redis.Dial("tcp", addr)
	if err != nil {
		return ps, err
	}
	ps.Conn = conn

	return ps, nil
}

func main() {
	args := parseCmdLine()
	if len(args) < 1 {
		fmt.Fprintln(os.Stderr, "Configuration file not specified")
		flag.Usage()
		os.Exit(2)
	}

	logger := initLogger()
	slog.SetDefault(logger)

	cfg, err := ReadConfig(args[0])
	if err != nil {
		abort("read configuration file", err)
	}

	client, err := redisConnect(cfg.Redis.Address)
	if err != nil {
		abort("Redis broker connect", err,
			slog.String("addr", cfg.Redis.Address))
	}

	ctx, stop := signal.NotifyContext(context.Background(),
		os.Interrupt, syscall.SIGTERM, syscall.SIGHUP)
	defer stop()

	// Create a Go chan for each archive.
	chMap := make(map[string]chan []byte, len(cfg.Redis.Archives))
	channels := make([]string, 0, len(cfg.Redis.Archives))
	for name := range cfg.Redis.Archives {
		chMap[name] = make(chan []byte, qLen)
		channels = append(channels, name)
	}

	// Start the Redis message subscriber goroutine
	ch := make(chan redis.Message, 4)
	errCh := make(chan error, 1)
	go func() {
		errCh <- rdsub.ReceiveMsgs(ctx, client, func(m redis.Message) bool {
			select {
			case ch <- m:
			default:
				slog.Error("message dropped",
					slog.String("channel", m.Channel))
			}
			return false
		}, logger)
	}()

	// Archive file pathname generator
	arName := archiveNamer(cfg.Params.Interval)

	// Open the active file database and process any "leftover" files
	// from the previous session. If the file is locked, we wait a
	// bit for it to be released.
	const indexDBName = "active.db"
	db, err := bbolt.Open(filepath.Join(cfg.Params.Topdir, indexDBName),
		0644,
		&bbolt.Options{Timeout: 2 * time.Second})
	if err != nil {
		slog.Error("cannot open index file", slog.Any("err", err))
		db = nil
	} else {
		reapFiles(db)
		initDb(db)
	}

	slog.Info("Redis Archiver starting", slog.String("version", Version))
	client.PSubscribe(redis.Args{}.AddFlat(channels)...)
	defer client.PUnsubscribe()

	for ctx.Err() == nil {
		t0 := time.Now()
		deadline := cfg.NextDeadline(t0)
		slog.Info("next deadline", slog.Time("t", deadline))
		arCtx, cancel := context.WithDeadline(ctx, deadline)
		g := new(errgroup.Group)

		// Start an archiver task for each topic
		for topic, arCfg := range cfg.Redis.Archives {
			// These assignments are needed for versions of Go < 1.22.
			//
			// https://go.dev/doc/faq#closures_and_goroutines
			ch := chMap[topic]
			arCfg := arCfg
			g.Go(func() error {
				return archiver(arCtx,
					filepath.Join(cfg.Params.Topdir, arName(t0, arCfg)),
					ch,
					arCfg, db)
			})
		}

		for arCtx.Err() == nil {
			select {
			case msg := <-ch:
				if next, ok := chMap[msg.Pattern]; ok {
					select {
					case next <- msg.Data:
					default:
					}
				}
			case <-arCtx.Done():
				// Deadline reached, rerun the outer loop once the archiving
				// tasks have finished.
				slog.Info("waiting for tasks to finish")
				if err := g.Wait(); err != nil {
					slog.Error("", slog.Any("err", err))
				}
				// Not strictly necessary since arCtx has already
				// been canceled, but it keeps go vet happy.
				cancel()
			case err := <-errCh:
				// Message subscriber goroutine returned an error
				slog.Error("", slog.Any("err", err))
				return
			case <-ctx.Done():
				client.PUnsubscribe()
				// Interrupt signal, exit program after tasks finish
				slog.Info("waiting for tasks to finish")
				if err := g.Wait(); err != nil {
					slog.Error("", slog.Any("err", err))
				}
				return
			}
		}

	}
}

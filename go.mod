module bitbucket.org/uwaploe/rdarchiver

require (
	github.com/alicebob/miniredis/v2 v2.33.0
	github.com/carlmjohnson/versioninfo v0.22.5
	github.com/gomodule/redigo v1.9.2
	go.etcd.io/bbolt v1.3.11
	golang.org/x/exp v0.0.0-20241009180824-f66d83c29e7c
	golang.org/x/sync v0.8.0
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/alicebob/gopher-json v0.0.0-20200520072559-a9ecdc9d1d3a // indirect
	github.com/yuin/gopher-lua v1.1.1 // indirect
	golang.org/x/sys v0.4.0 // indirect
)

go 1.22.0

toolchain go1.23.1

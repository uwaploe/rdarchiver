version: '3'

env:
  CGO_ENABLED: '0'

vars:
  VERSION:
    sh: git describe --tags --always --dirty --match=v* 2> /dev/null || echo v0
  DATE:
    sh: date +'%FT%T'
  PWD:
    sh: pwd
  LDFLAGS: "-s -w -X main.Version={{.VERSION}} -X main.BuildDate={{.DATE}}"

tasks:
  build-exe:
    desc: "Build the executable"
    internal: true
    vars:
      TARGET: "{{.NAME}}_{{.GOOS}}_{{.GOARCH}}{{with .GOARM}}_{{.}}{{end}}"
    cmds:
      - mkdir -p dist/{{.TARGET}}
      - go build -o dist/{{.TARGET}} -ldflags '{{.LDFLAGS}}'

  build:
    cmds:
      - task: build-exe
        vars:
          GOOS: "{{default OS .GOOS}}"
          GOARCH: "{{default ARCH .GOARCH}}"
          NAME: "{{base .PWD}}"

  snapshot:
    desc: Create a snapshot package
    cmds:
      - goreleaser release --clean --snapshot

  release:
    desc: Create a release package
    vars:
      UNPUSHED:
        sh: git rev-list @{u}..@ --count
    cmds:
      - bumpvers -m "New release" {{.CLI_ARGS}}
      - git push origin --tags
      - goreleaser release --clean
    preconditions:
      - goreleaser release --clean --snapshot
      - sh: git diff --quiet
        msg: "Repository is not clean"
      - sh: '[ {{.UNPUSHED}} -eq 0 ]'
        msg: "{{.UNPUSHED}} unpushed commit(s)"

  mod:
    desc: Downloads and tidy Go modules
    cmds:
      - go mod download
      - go mod tidy

  lint:
    desc: Run various Go linters
    cmds:
      - golangci-lint run ./...

  clean:
    desc: Cleans temp files and folders
    cmds:
      - rm -rf dist/
      - rm -rf tmp/
      - rm -f "{{base .PWD}}"

  default:
    cmds:
      - task -l
    silent: true

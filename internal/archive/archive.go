// Package archive writes data records to an underlying Writer
package archive

import (
	"fmt"
	"io"
	"time"
)

type options struct {
	delim   []byte
	sep     []byte
	header  []byte
	addTime bool
	timeFmt string
}

// Option is a configurable option for a [Writer].
type Option func(opts *options) error

// WithDelim returns an Option which sets the delimiter string to use
// between each record. The default value is "\n".
func WithDelim(delim string) Option {
	return func(opts *options) error {
		opts.delim = []byte(delim)
		return nil
	}
}

// WithTimestamp returns an Option which prefixes a timestamp string to
// every record, sep specifies the string which separates the timestamp from
// the record. The timestamp uses the UTC timezone.
func WithTimestamp(sep string) Option {
	return func(opts *options) error {
		opts.sep = []byte(sep)
		opts.addTime = true
		return nil
	}
}

// WithTimeFormat returns an Option which sets the [time.Time] layout string
// for the added timestamp. The default is RFC3339 with millisecond
// precision.
func WithTimeFormat(layout string) Option {
	return func(opts *options) error {
		opts.timeFmt = layout
		return nil
	}
}

// WithHeader returns an Option which sets a header string for the
// output. The header will be written before the first record and will
// separated from the first record by the delimiter.
func WithHeader(hdr string) Option {
	return func(opts *options) error {
		opts.header = []byte(hdr)
		return nil
	}
}

type Writer struct {
	w          io.Writer
	opts       options
	hdrWritten bool
}

// NewWriter returns a new [Writer]. Writes to the returned writer are
// written to w followed by the optional delimiter string. Each record can
// optionally be prefixed with a timestamp.
func NewWriter(w io.Writer, opts ...Option) (*Writer, error) {
	wr := &Writer{
		w: w,
		opts: options{
			delim:   []byte("\n"),
			timeFmt: "2006-01-02T15:04:05.999Z07:00",
		},
	}
	for _, opt := range opts {
		if err := opt(&wr.opts); err != nil {
			return nil, err
		}
	}

	return wr, nil
}

// Write writes b to the underlying [io.Writer] followed by the record
// delimiter.
func (wr *Writer) Write(b []byte) (int, error) {
	if !wr.hdrWritten {
		if wr.opts.header != nil && len(wr.opts.header) > 0 {
			wr.w.Write(wr.opts.header)
			_, err := wr.w.Write(wr.opts.delim)
			if err != nil {
				return 0, err
			}
		}
		wr.hdrWritten = true
	}

	if wr.opts.addTime {
		fmt.Fprintf(wr.w, "%s%s", time.Now().UTC().Format(wr.opts.timeFmt), wr.opts.sep)
	}

	n, _ := wr.w.Write(b)
	_, err := wr.w.Write(wr.opts.delim)

	return n, err
}

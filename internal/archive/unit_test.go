package archive

import (
	"bytes"
	"testing"
)

func TestWriting(t *testing.T) {
	tests := []struct {
		in   []string
		opts []Option
		out  string
	}{
		{
			in:  []string{"rec1", "rec2", "rec3"},
			out: "rec1\nrec2\nrec3\n",
		},
		{
			in:   []string{"rec1", "rec2", "rec3"},
			opts: []Option{WithDelim("\r\n")},
			out:  "rec1\r\nrec2\r\nrec3\r\n",
		},
		{
			in:   []string{"rec1", "rec2", "rec3"},
			opts: []Option{WithHeader("# header line")},
			out:  "# header line\nrec1\nrec2\nrec3\n",
		},
	}

	var b bytes.Buffer
	for _, tt := range tests {
		w, err := NewWriter(&b, tt.opts...)
		if err != nil {
			t.Fatal(err)
		}
		for _, rec := range tt.in {
			w.Write([]byte(rec))
		}
		if got := b.String(); got != tt.out {
			t.Errorf("bad result; expected %q, got %q", tt.out, got)
		}
		b.Reset()
	}
}

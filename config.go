package main

import (
	"fmt"
	"os"
	"time"

	"bitbucket.org/uwaploe/rdarchiver/internal/archive"
	yaml "gopkg.in/yaml.v3"
)

type sysConfig struct {
	Params sysParams `yaml:"params"`
	Redis  rdConfig  `yaml:"redis"`
}

func (c sysConfig) NextDeadline(t time.Time) time.Time {
	return t.Truncate(c.Params.Interval).Add(c.Params.Interval)
}

type sysParams struct {
	Topdir   string        `yaml:"topdir"`
	Interval time.Duration `yaml:"interval"`
}

type arConfig struct {
	Prefix  string `yaml:"prefix"`
	Ext     string `yaml:"ext"`
	Delim   string `yaml:"delim"`
	Linkdir string `yaml:"linkdir"`
	Header  string `yaml:"header"`
	AddTime bool   `yaml:"add_time"`
	Sep     string `yaml:"sep"`
}

func (c arConfig) ArchiveOptions(isNew bool) []archive.Option {
	opts := make([]archive.Option, 0)
	opts = append(opts, archive.WithDelim(c.Delim))
	if isNew {
		opts = append(opts, archive.WithHeader(c.Header))
	}
	if c.AddTime {
		opts = append(opts, archive.WithTimestamp(c.Sep))
	}

	return opts
}

type rdConfig struct {
	Address  string              `yaml:"address"`
	Archives map[string]arConfig `yaml:"archives"`
}

func (c *sysConfig) Load(contents []byte) error {
	return yaml.Unmarshal(contents, c)
}

func ReadConfig(path string) (sysConfig, error) {
	var cfg sysConfig

	contents, err := os.ReadFile(path)
	if err != nil {
		return cfg, fmt.Errorf("read %s: %w", path, err)
	}
	err = cfg.Load(contents)

	return cfg, err
}

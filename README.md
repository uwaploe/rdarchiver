# Redis Channel Archiver

Rdarchiver is a tool to archive the contents of messages published to one or more Redis channels. The messages are stored in a series of files organized by date and time.

## Configuration File

A [YAML](https://en.wikipedia.org/wiki/YAML) file is used for configuration, below is a simple example.

``` yaml
params:
  topdir: /data/TEST
  interval: "6h"
redis:
  address: localhost:6379
  archives:
    # There is one archive entry for each channel, the
    # entry key is the channel name.
    data.test:
      # Archive file prefix
      prefix: test
      # Archive file extension
      ext: bin
      # Record delimiter (none)
      delim: ""
      # Directory to link each file into when closed
      linkdir: /data/OUTBOX
    data.eng:
      prefix: eng
      ext: csv
      delim: "\n"
      linkdir: /data/OUTBOX
```


### Channel Configuration Directives

#### prefix (string: required)

Prefix prepended to each filename.

#### delim (string: required)

Delimiter to write after each record

#### ext (string: optional, default: "dat")

Filename extension

#### header (string: optional)

Header record written at the start of each file.

#### linkdir (string: optional)

Directory to link each completed file into. This is useful to signal other processes that a new file is available.

## Archive Organization

The files are organized in a time-based directory tree with the following naming schemes (DDD is the year-day, 001-366).

### Interval between 1 hour and 1 day

```
TOPDIR/YYYY/DDD/PREFIX-YYYYmmdd-HH.EXT
```

### Interval less than 1 hour

```
TOPDIR/YYYY/DDD/PREFIX-YYYYmmdd-HHMM.EXT
```

### Interval greater than or equal to 1 day

```
TOPDIR/YYYY/DDD/PREFIX-YYYYmmdd.EXT
```

## Systemd Service

The program can be installed as a [Systemd](https://systemd.io) service under Linux by copying the contents of the `systemd` subfolder to `/lib/systemd/system` (this is done automatically when installing the Debian package). Configuration files must be installed under `/usr/local/etc/rdarchiver`.

package main

import (
	"bufio"
	"context"
	"fmt"
	"os"
	"path/filepath"
	"time"

	"log/slog"

	"bitbucket.org/uwaploe/rdarchiver/internal/archive"
	"go.etcd.io/bbolt"
)

type FilePathFunc func(time.Time, arConfig) string

// Return a file naming function which will generate a pathname based on
// the current time and the file creation interval.
func archiveNamer(interval time.Duration) FilePathFunc {
	switch {
	case interval >= (time.Hour * 24):
		return FilePathFunc(func(t time.Time, cfg arConfig) string {
			if t.IsZero() {
				t = time.Now().UTC()
			}
			if cfg.Ext == "" {
				cfg.Ext = "dat"
			}
			return fmt.Sprintf("%d/%03d/%s-%s.%s",
				t.Year(),
				t.YearDay(),
				cfg.Prefix,
				t.Format("20060102"), cfg.Ext)
		})
	case interval >= time.Hour:
		return FilePathFunc(func(t time.Time, cfg arConfig) string {
			if t.IsZero() {
				t = time.Now().UTC()
			}
			t = t.Truncate(interval)
			if cfg.Ext == "" {
				cfg.Ext = "dat"
			}
			return fmt.Sprintf("%d/%03d/%s-%s.%s",
				t.Year(),
				t.YearDay(),
				cfg.Prefix,
				t.Format("20060102-15"), cfg.Ext)
		})
	default:
		return FilePathFunc(func(t time.Time, cfg arConfig) string {
			if t.IsZero() {
				t = time.Now().UTC()
			}
			t = t.Truncate(interval)
			if cfg.Ext == "" {
				cfg.Ext = "dat"
			}
			return fmt.Sprintf("%d/%03d/%s-%s.%s",
				t.Year(),
				t.YearDay(),
				cfg.Prefix,
				t.Format("20060102-1504"), cfg.Ext)
		})
	}
}

// Support for linking files into a common directory. This provides
// a more robust API than a function which accepts the directory name
// and file name (i.e. two strings).
//
// See: https://dave.cheney.net/2019/09/24/be-wary-of-functions-which-take-several-parameters-of-the-same-type
//
type linkDir string

func (d linkDir) AddFile(path string) error {
	os.MkdirAll(string(d), 0755)
	linkname := filepath.Join(string(d), filepath.Base(path))
	os.Remove(linkname)
	return os.Link(path, linkname)
}

func (d linkDir) String() string {
	return string(d)
}

// If there are any active files left from the previous session, link them into
// their corresponding link directory (if any).
func reapFiles(db *bbolt.DB) error {
	return db.View(func(tx *bbolt.Tx) error {
		b := tx.Bucket([]byte("active"))
		if b == nil {
			return nil
		}

		b.ForEach(func(k, v []byte) error {
			if len(v) == 0 {
				// No link directory for this file, this
				// should never happen.
				return nil
			}

			path := string(k)
			dir := linkDir(v)
			slog.Info("linking file", slog.String("path", path),
				slog.String("dir", dir.String()))

			if err := dir.AddFile(path); err != nil {
				slog.Error("link failed", slog.Any("err", err))
			}
			return nil
		})
		return nil
	})
}

// Initialize the database containing the active file list
func initDb(db *bbolt.DB) error {
	// Unconditionally delete the "active" bucket and
	// recreate it.
	return db.Update(func(tx *bbolt.Tx) error {
		tx.DeleteBucket([]byte("active"))
		_, err := tx.CreateBucket([]byte("active"))
		return err
	})
}

func newArchiveFile(path string) (*os.File, bool, error) {
	// Check if file already exists
	_, err := os.Stat(path)
	newfile := os.IsNotExist(err)

	if newfile {
		// Create all directories if needed. We can safely ignore the
		// returned error because it will be caught when we try to
		// open the file.
		_ = os.MkdirAll(filepath.Dir(path), 0755)
	}

	f, err := os.OpenFile(path, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	return f, newfile, err
}

// Archiver accepts byte slices from src and appends them as individual
// records to the file at path. The function returns if an error occurs, ctx
// is canceled, or src is closed. If db is non-nil it is used to log
// active files.
func archiver(ctx context.Context, path string, src <-chan []byte, cfg arConfig,
	db *bbolt.DB) error {
	f, isNew, err := newArchiveFile(path)
	if err != nil {
		return err
	}
	slog.Info("open archive", slog.String("path", path), slog.Bool("is_new", isNew))

	// Add to the active file list
	if db != nil && cfg.Linkdir != "" {
		db.Update(func(tx *bbolt.Tx) error {
			b := tx.Bucket([]byte("active"))
			return b.Put([]byte(path), []byte(cfg.Linkdir))
		})
	}

	wbuf := bufio.NewWriter(f)
	defer wbuf.Flush()
	w, err := archive.NewWriter(wbuf, cfg.ArchiveOptions(isNew)...)
	if err != nil {
		return err
	}

	// On return, close the file, remove it if empty, and optionally link
	// it into cfg.Linkdir
	defer func(d string) {
		var ld linkDir = linkDir(d)

		err := f.Close()
		if err != nil {
			slog.Error("close failed", slog.Any("err", err),
				slog.String("path", path))
			return
		}
		// Remove empty files
		info, err := os.Stat(path)
		slog.Info("close archive", slog.String("path", path),
			slog.Int64("size", info.Size()))

		if err == nil && info.Size() == 0 {
			os.Remove(path)
			return
		}

		if d != "" {
			err = ld.AddFile(path)
			// If the linking was successful, remove the filename
			// from the active file list.
			if err == nil && db != nil {
				db.Update(func(tx *bbolt.Tx) error {
					b := tx.Bucket([]byte("active"))
					return b.Delete([]byte(path))
				})
			}
		}

	}(cfg.Linkdir)

	for {
		select {
		case <-ctx.Done():
			return ctx.Err()
		case rec, ok := <-src:
			if !ok {
				// Channel closed
				return nil
			}
			_, _ = w.Write(rec)
			if err := wbuf.Flush(); err != nil {
				return err
			}
		}
	}
}

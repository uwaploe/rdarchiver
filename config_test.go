package main

import "testing"

func TestLoadConfig(t *testing.T) {
	const cfgfile = `
params:
  topdir: /data/TEST
  interval: "15m"
redis:
  address: localhost:6379
  archives:
    data.test1:
      prefix: test1
      ext: csv
      header: "time,col1,col2,col3"
      delim: "\n"
      linkdir: /data/OUTBOX
    data.test2:
      prefix: test2
      header: "time,col1,col2"
      delim: "\n"
      ext: csv
      linkdir: /data/OUTBOX
    data.test3:
      prefix: test3
      delim: ""
      ext: bin
      linkdir: /data/OUTBOX
`
	cfg := sysConfig{}
	cfg.Load([]byte(cfgfile))

	if n := len(cfg.Redis.Archives); n != 3 {
		t.Errorf("Wrong archive count; expected 3, got %d", n)
	}
}
